# README #

This is a script to email a list of configured emails, when an earlier Auslaenderbehoerde termin appears on LABO website.


See also: Burgerbot, a burgeramt termin catching tool: https://gist.github.com/pbock/3ab260f3862c350e6b5f

Moscow German Consulate visa application status parser: https://github.com/urakozz/visa-status