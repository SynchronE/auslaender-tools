#!/usr/bin/env php
<?php
require_once 'phpmailer/class.phpmailer.php';
require_once 'phpmailer/class.smtp.php';
require 'vendor/autoload.php';

$dotenv = new Dotenv\Dotenv(__DIR__);
$dotenv->load();

date_default_timezone_set('Europe/Berlin');
const ACCESS_URL = 'https://formular.berlin.de/jfs/findform?shortname=OTVBerlin&formtecid=4&areashortname=LABO&termin=1&dienstleister=121885&anliegen[]=324659&herkunft=1';
const BASEURL = 'https://formular.berlin.de/xima-forms-29/';
const FETCH_MONTHS = 6;


    if($argc < 3){
        $session = get_sessiondata();
        vprintf('Got UUID %s (JSESSIONID: %s)'.PHP_EOL, array_values($session));
        echo 'Getting data page...';

        //feed_session(1,$session); #switch lang
        feed_session(11, $session); #set booking
        feed_session(21, $session); #choose citizenship //hardcoded to RU
        feed_session(22, $session, ['cobFamAngInBerlin' => 'Nein']); #spouse citizenship //cobFamAngInBerlin
       // feed_session(23, $session, ['cobAnliegen' => 324659]); # Blue Card
        feed_session(23, $session, ['cobAnliegen' => 305289]); #set application type: Residence for wife/husband, parents and children (sections 27-
        feed_session(24, $session); #spouse permit
        feed_session(25, $session); #confirm agreement
        feed_session(31, $session); #preevious permit
        feed_session(32, $session); #fill the infos
    }else{
        $session = array('uuid' => $argv[1], 'id' => $argv[2]);
    }

    $statuses = array();
    $d        = new DateTime();
    $attempts = 0;

    while(count($statuses) == 0 && $attempts++ < FETCH_MONTHS){
        $statuses = find_statuses(
            feed_session(
                create_query(
                    $d->format('m'),
                    $d->format('Y')
                ),
                $session
            )
        );
        $d->add(new DateInterval('P1M'));
    }

    $curStat = current($statuses);
    if($curStat !== false){
        echo 'earliest is: '.$curStat->format('l, d-M-y').PHP_EOL;

        $earliest_parsed = (float)($curStat->format('U'));

        connect_db();
        log_date($earliest_parsed);
        //send_notification($earliest_parsed);
        exit(0);
    }else{
        echo "No termin found";
        exit(1);
    }



function create_query($month,$year)
{
    //subtract 1 Day to get 'next' for that
    $d = new DateTime(sprintf('01-%d-%d', $month, $year));
    $d = $d->sub(new DateInterval('P1D'));

    return array(
        'monthn' => $d->format('m'),
        'year' => $d->format('Y'),
        '-UASource'=>'labnextMonth',
        '-UAEvent'=>'showpage',
        '-UAParam'=>''
    );
}

function find_statuses($text)
{
    preg_match('/name="year" value="(\d{4})"/',$text,$year);
    $year = $year[1];
    preg_match('/name="monthn" value="(\d+)"/i',$text,$month);
    $month = $month[1]++;
    if($month > 12){$month = 1; $year++;}

    echo 'got '.$month.' of '.$year.'...';

    preg_match_all('/txt(Mo|Di|Mi|Do|Fr|Sa|So)Status_([0-5])" value="1.+\<span\>(\d+)\</imsU', $text, $matches, PREG_SET_ORDER);

    return array_map(function($day) use ($year, $month){
        return new DateTime(sprintf('%d-%d-%d',$day[3],$month, $year));
    },$matches);
}

function is_error($s){
    return (bool) preg_match('/\<td class="error"/imsU',$s);
}

function feed_session($postData, $session, $extra = []){
    $c = curl_init(BASEURL.'nextpage/'.$session['uuid'].'/');
    curl_setopt($c, CURLOPT_COOKIE, 'JSESSIONID='.$session['id']);
    curl_setopt($c, CURLOPT_POST, true);

    if(is_int($postData)){
        parse_str(file_get_contents('stage'.$postData.'.urlenc'), $postData);
    }
    $postData = $extra + $postData;

    curl_setopt($c, CURLOPT_POSTFIELDS, $postData);
    curl_setopt($c, CURLOPT_HTTPHEADER, array(
        'User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
    ));
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    $v = curl_exec($c);
    curl_close($c);
    //file_put_contents(substr('stage'.preg_replace('/[^a-zA-Z0-9]/', '', is_array($postData) ? implode('_',$postData) : $postData),0,128).'.html', $v);
    if(is_error($v)){ die('Error @ stage '.$postData.'!'); }
    return $v;
}

function get_sessiondata(){
    $c = curl_init(ACCESS_URL);
    curl_setopt($c, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($c, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($c, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($c, CURLOPT_HTTPHEADER, array(
        'User-Agent: Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36',
    ));

    $response = curl_exec($c);

    if($e = curl_errno($c)){
        echo $e.': '.curl_error($c);
        die();
    }

    preg_match('/nextpage\/([0-9a-f-]+)\/;jsessionid=([A-F0-9]+)"/',
        $response,$match);
    array_shift($match);
    return array_combine(array('uuid','id'),$match);
}

function connect_db(){
    global $mysqli;
    $mysqli = mysqli_connect(getenv('DB_HOST'),getenv('DB_USER'),getenv('DB_PASS'),getenv('DB_NAME'));
}
/**
 * Log date
 *
 * @param int $date
 */
function log_date($date) {
    global $mysqli;
    $mysqli->query('INSERT INTO `success_log` (`appointment_date`, `parsed_time`) VALUES ('.$date.', '.time().')');
}

/**
 * Get last notification time
 *
 * @return int|null
 */
function get_last_sent_date() {
    global $mysqli;
    $q = $mysqli->query('SELECT `value` FROM `sys_params` WHERE `param` = \'last_sent_appointment_date\' LIMIT 1');
    $result = $q->fetch_assoc();
    if (!empty($result['value'])) {
        return (int)$result['value'];
    } else {
        return null;
    }
}

/**
 * Get last parsed appointment timestamp and timestamp when it was parsed
 *
 * @return array|null
 */
function get_last_parsed_date() {
	global $mysqli;
	$q = $mysqli->query('SELECT `appointment_date`, `parsed_time` FROM `success_log` ORDER BY `id` DESC LIMIT 1');
	$result = $q->fetch_assoc();
	if (!empty($result)) {
		return $result;
	} else {
		return null;
	}
}

/**
 * Send notification
 *
 * @param int $date
 */
function send_notification($date) {
    global $mysqli;
    $last_sent_date = get_last_sent_date();
    if(!$last_sent_date || $date != $last_sent_date) {
        $email = new PHPMailer();

        $email->isSMTP();
        $email->Host = getenv('SMTP_HOST');
        $email->SMTPAuth = true;
        $email->From = getenv('SMTP_FROM');
        $email->Username = getenv('SMTP_USERNAME') ?: $email->From;
        $email->Password = getenv('SMTP_PASSWORD');
        $email->SMTPSecure = 'tls';
        $email->FromName = 'AB Bot';

        $recipients = array(
            getenv('NOTIFICATION_EMAIL')
        );
        $extra_recipients = explode(',', getenv('NOTIFICATION_EMAIL_EXTRA'));
        $email->Subject = 'Auslaenderbehoerde Notification. Nearest appointment date changed!';
        $email->Body = 'Current nearest date is '.format_date($date);
        foreach($recipients as $recipient){
            $email->AddAddress($recipient);
        }
        $result = $email->Send();
        if ($result) {
            $mysqli->query('REPLACE INTO `sys_params` (`param`, `value`) VALUES (\'last_sent_appointment_date\', \''.$date.'\')');
            if ($last_sent_date && $date < $last_sent_date) {

                foreach($extra_recipients as $recipient){
                    $email->AddAddress($recipient);
                }
                $email->Send();
            }
        }else{
            echo "Failed sending an email: ".$email->ErrorInfo;
        }
    }
}

/**
 * Log error
 *
 * @param string $message
 */
function trigger_parsing_error($message) {
	$time = time();
	log_error($message, $time);
	send_error_notification($message, $time);
}

/**
 * Log error
 *
 * @param string $message
 * @param int $time
 */
function log_error($message, $time) {
	global $mysqli;
	$mysqli->query('INSERT INTO `error_log` (`message`, `time`) VALUES (\''.$message.'\', '.$time.')');
}

/**
 * Send error notification
 *
 * @param string $message
 * @param int $time
 */
function send_error_notification($message, $time) {
	global $mysqli;
	$last_error = get_last_error();
	if(!$last_error || ($time - $last_error['time']) > 600) {
        $email = new Email();
        $error_recipients = array(
            'alex.sarajkin@gmail.com',
        );
		$subject = 'SPB German Embassy Notification. Parsing error!';
		$message = 'Parsing error: '.$message;
        $result = $email->mail($error_recipients, $subject, $message);
        if ($result) {
		    $mysqli->query('REPLACE INTO `sys_params` (`param`, `value`) VALUES (\'last_sent_error_time\', \''.$time.'\')');
        }
	}
}

/**
 * Count Errors
 *
 * @return array
 */
function get_last_error() {
	global $mysqli;
	$q = $mysqli->query('SELECT `message`, `time` FROM `error_log` ORDER BY `id` DESC LIMIT 1');
	$result = $q->fetch_assoc();
	if (!empty($result)) {
		return $result;
	} else {
		return 0;
	}
}

/**
 * Count Errors
 *
 * @return int
 */
function count_errors() {
	global $mysqli;
	$q = $mysqli->query('SELECT COUNT(*) AS `err_num` FROM `error_log`');
	$result = $q->fetch_assoc();
	if (!empty($result['err_num'])) {
		return (int)$result['err_num'];
	} else {
		return 0;
	}
}

/**
 * Format date
 *
 * @param int $timestamp
 *
 * @return string
 */
function format_date($timestamp) {
    return date('F d, Y', $timestamp);
}

/********************************************************
create table `success_log` (
    `id` int(10) unsigned not null auto_increment,
    `appointment_date` int(10) unsigned not null,
    `parsed_time` int(10) unsigned not null,
    primary key (`id`)
) engine='InnoDB';

create table `error_log` (
    `id` int(10) unsigned not null auto_increment,
    `message` varchar(255) not null,
    `time` int(10) unsigned not null,
    primary key (`id`)
) engine='InnoDB';

create table `sys_params` (
    `param` varchar(255) not null,
    `value` varchar(255) not null,
    unique key (`param`)
) engine='InnoDB';
********************************************************/
